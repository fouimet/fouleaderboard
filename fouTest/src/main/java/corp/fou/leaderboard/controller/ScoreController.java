/**
 * 
 */
package corp.fou.leaderboard.controller;

import java.util.Date;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import corp.fou.leaderboard.entity.Score;
import corp.fou.leaderboard.repository.ScoreRepository;

/**
 * @author Frederick Ouimet
 * 
 */
@Controller
@RequestMapping("json/scores")
public class ScoreController {
	private static final Logger logger = LoggerFactory.getLogger(ScoreController.class);

	@Resource
	private ScoreRepository scoreRepository;

	@RequestMapping(value = "post", params = { "name", "seconds" })
	@ResponseBody
	public boolean postScore(@RequestParam String name, @RequestParam Integer seconds) {
		logger.debug("ScoreController.postScore() ... name[" + name + "], seconds[" + seconds + "]");
		boolean rv = false;
		try {
			Score score = new Score();
			score.setName(name);
			score.setSeconds(seconds);
			score.setPostTime(new Date());

			scoreRepository.save(score);
			rv = true;
		} catch (Exception e) {
			logger.debug("ScoreController.postScore() ...", e);
		}
		
		return rv;
	}
}
