/**
 * 
 */
package corp.fou.leaderboard.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import corp.fou.leaderboard.entity.Score;
import corp.fou.leaderboard.repository.ScoreRepository;

/**
 * @author Frederick Ouimet
 *
 */
@Service
public class ScoreService {
	private static final Logger logger = LoggerFactory.getLogger(ScoreService.class);
	@Resource
	private ScoreRepository scoreRepository;
	
	public List<Score> getScores() {
		logger.debug("ScoreService.getScores()...");

		List<Order> sorting = new ArrayList<Sort.Order>(2);
		sorting.add(new Order(Direction.ASC, "seconds"));
		sorting.add(new Order(Direction.ASC, "postTime"));
		Sort sort = new Sort(sorting);
		return scoreRepository.findAll(sort);
	}
	
	public void clearScores() {
		scoreRepository.deleteAll();
	}
}
