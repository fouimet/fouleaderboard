/**
 * 
 */
package corp.fou.leaderboard.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import corp.fou.leaderboard.entity.Score;

/**
 * @author Frederick Ouimet
 *
 */
public interface ScoreRepository extends JpaRepository<Score, Integer> {

}
