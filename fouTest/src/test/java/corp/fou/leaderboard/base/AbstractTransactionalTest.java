/**
 * 
 */
package corp.fou.leaderboard.base;

import org.springframework.transaction.annotation.Transactional;

/**
 * @author Frederick Ouimet
 *
 */
@Transactional
public abstract class AbstractTransactionalTest extends AbstractTest {

}
