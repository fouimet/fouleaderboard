package corp.fou.leaderboard.service;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import corp.fou.leaderboard.base.AbstractTransactionalTest;
import corp.fou.leaderboard.entity.Score;

public class ScoreServiceTest extends AbstractTransactionalTest {
	@Resource
	private ScoreService scoreService;
	
	@Test
	public void testGetScores() {
		List<Score> scores = scoreService.getScores();
		Assert.assertTrue(scores != null);
		Assert.assertTrue(scores.size() > 0);
	}

}
