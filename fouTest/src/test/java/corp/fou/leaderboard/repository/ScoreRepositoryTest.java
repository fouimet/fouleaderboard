/**
 * 
 */
package corp.fou.leaderboard.repository;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import corp.fou.leaderboard.base.AbstractTransactionalTest;
import corp.fou.leaderboard.entity.Score;

/**
 * @author Frederick Ouimet
 *
 */
public class ScoreRepositoryTest extends AbstractTransactionalTest {
	@Resource
	private ScoreRepository scoreRepository;
	
	@Test
	public void testSave() {
		long count = scoreRepository.count();
		Score s = new Score();
		s.setName("Fred");
		s.setSeconds(76);
		s.setPostTime(new Date());
		
		scoreRepository.save(s);
		
		Assert.assertTrue(scoreRepository.count() == count+1);
	}

}
